Examples and notes for my speed talk on Spring Data (JPA) Repositories

***

https://docs.spring.io/spring-data/jpa/docs/current/reference/html/

Interfaces

* Repository<T, ID>
* CrudRepository<T, ID> extends Repository<T, ID>
* PagingAndSortingRepository<T, ID> extends CrudRepository<T, ID>

Method name mapping:
https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation

Sorting and Paging
==================

Query with _Sort(direction, property name)_ returns list

Query with _PageRequest(page number, page size[, sort])_ can return

* List
* Slice extends List (elements, number, hasNext/hasPrevious, sort, ...)
* Page extends Slice (number of pages)

!! Check the runtime results (see com.example.springdatajpademo.UserRepository.findAll)

More features 
=============

Supported return types

* com.google.common.base.Optional
* scala.Option
* io.vavr.control.Option
* Kotlin Nullable
* Streams

Asynchronous execution

* java.util.concurrent.Future
* java.util.concurrent.CompletableFuture
* org.springframework.util.concurrent.ListenableFuture

Query methods

* Named queries
* Criteria API
* Query dsl
* Query by Example

Auditing

* @CreatedBy
* @LastModifiedBy

Other stuff

* Query Hints
* Fetch- and LoadGraphs
* Projections (DTOs)
* Stored Procedures

Spring Data REST

* Generated REST API

Other Databases

* cassandra
* couchbase
* elasticsearch
* hadoop
* ldap
* mongodb 
* neo4j
* redis
* solr
