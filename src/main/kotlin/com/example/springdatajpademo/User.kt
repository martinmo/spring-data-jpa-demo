package com.example.springdatajpademo

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.NamedQuery

@Entity
@NamedQuery(name = "User.findByLastNameNamedQuery", query = "from User where lastName = :lastName")
data class User(
        var firstName: String,
        var lastName: String,
        @Id @GeneratedValue private val _id: Long? = null
) {
    val id get() = _id!!
    val persistent get() = _id != null
}