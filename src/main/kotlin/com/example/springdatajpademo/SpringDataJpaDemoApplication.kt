package com.example.springdatajpademo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringDataJpaDemoApplication

fun main(args: Array<String>) {
    runApplication<SpringDataJpaDemoApplication>(*args)
}
