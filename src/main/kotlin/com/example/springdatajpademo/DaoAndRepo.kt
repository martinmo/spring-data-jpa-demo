package com.example.springdatajpademo

import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Slice
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager

@Service
@Transactional
class ClassicUserDao(private val em: EntityManager) {

    fun createOrUpdate(user: User): User = if (user.persistent) {
        em.merge(user)
    } else {
        em.persist(user)
        user
    }

    fun delete(user: User) = em.remove(user)

    fun findById(id: Long): User? = em.find(User::class.java, id)

    fun findByLastName(lastName: String): List<User> =
            em.createQuery("from User where lastName = :lastName", User::class.java)
                    .setParameter("lastName", lastName)
                    .resultList

    fun findByLastNameNamedQuery(lastName: String): List<User> =
            em.createNamedQuery("User.findByLastNameNamedQuery", User::class.java)
                    .setParameter("lastName", lastName)
                    .resultList

    fun findByLastNameOrderByFirstName(lastName: String, ordering: SortOrder): List<User> =
            em.createQuery("from User where lastName = :lastName order by firstName " + ordering.name, User::class.java)
                    .setParameter("lastName", lastName)
                    .resultList

    fun findAll(page: Int = 0, perPage: Int = 1000): List<User> {
        val query = em.createQuery("from User", User::class.java)
        query.maxResults = perPage
        query.firstResult = page * perPage
        return query.resultList
    }

}

interface UserRepository : CrudRepository<User, Long> {
    fun findByLastName(lastName: String): List<User>

    fun findByLastNameNamedQuery(lastName: String): List<User>

    fun findByLastName(lastName: String, ordering: Sort = Sort(Sort.Direction.ASC, "firstName")): List<User>

    @Query("from User where lastName = :lastName")
    fun findByLastNameByQuery(lastName: String): List<User>

    @Query("from User") // if removed it returns a page, which triggers a count query
    fun findAll(pageable: Pageable): Slice<User>
}

enum class SortOrder {
    ASC, DESC
}
