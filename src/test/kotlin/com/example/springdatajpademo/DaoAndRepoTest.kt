package com.example.springdatajpademo

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.SliceImpl
import org.springframework.data.domain.Sort
import org.springframework.data.domain.Sort.Direction.ASC
import org.springframework.data.domain.Sort.Direction.DESC
import java.util.Optional

class DaoAndRepoTest : DbTest() {

    @Test
    fun `can create`() {
        val hinz = dao.createOrUpdate(User(firstName = "Hinz", lastName = "Kunz"))
        assertThat(em.find(User::class.java, hinz.id)).isEqualTo(hinz)

        val springHinz = repo.save(User(firstName = "Spring Hinz", lastName = "Kunz"))
        assertThat(em.find(User::class.java, springHinz.id)).isEqualTo(springHinz)
    }

    @Test
    fun `can delete`() {
        val user = aUser()
        dao.delete(user)
        assertThat(dao.findAll(0, 1)).isEmpty()

        val repoUser = aUser()
        repo.delete(repoUser)
        assertThat(repo.findAll()).isEmpty()
    }

    @Test
    fun `can find by id`() {
        val user = aUser()

        assertThat(dao.findById(user.id)).isEqualTo(user)
        assertThat(repo.findById(user.id).asNullable()).isEqualTo(user)
    }

    @Test
    fun `can find by last name`() {
        aUser(lastName = "other last name")
        val kunz = aUser(lastName = "Kunz")

        assertThat(dao.findByLastName("Kunz")).containsExactly(kunz)
        assertThat(dao.findByLastNameNamedQuery("Kunz")).containsExactly(kunz)

        assertThat(repo.findByLastName("Kunz")).containsExactly(kunz)
        assertThat(repo.findByLastNameNamedQuery("Kunz")).containsExactly(kunz)
        assertThat(repo.findByLastNameByQuery("Kunz")).containsExactly(kunz)
    }

    @Test
    fun `can find by last name sorted`() {
        val anna = aUser(firstName = "Anna", lastName = "Kunz")
        val zora = aUser(firstName = "Zora", lastName = "Kunz")

        assertThat(dao.findByLastNameOrderByFirstName("Kunz", SortOrder.DESC)).containsExactly(zora, anna)
        assertThat(dao.findByLastNameOrderByFirstName("Kunz", SortOrder.ASC)).containsExactly(anna, zora)

        assertThat(repo.findByLastName("Kunz", Sort(DESC, "firstName"))).containsExactly(zora, anna)
        assertThat(repo.findByLastName("Kunz", Sort(ASC, "firstName"))).containsExactly(anna, zora)
    }

    @Test
    fun `can list`() {
        (1..5).forEach {
            aUser(lastName = it.toString())
        }

        assertThat(dao.findAll(0, 3)).hasSize(3)
        assertThat(dao.findAll(1, 3)).hasSize(2)

        assertThat(repo.findAll(PageRequest.of(0, 3))).hasSize(3)
        assertThat(repo.findAll(PageRequest.of(1, 3))).hasSize(2)
    }

    @Test
    fun `can paginate with slice`() {
        (1..5).forEach {
            aUser(lastName = it.toString())
        }

        val slice1 = repo.findAll(PageRequest.of(0, 3, Sort(ASC, "lastName")))

        assertThat(slice1).hasSize(3)
        assertThat(slice1.isFirst).isTrue()
        assertThat(slice1.isLast).isFalse()

        val nextPageRequest = slice1.nextPageable()
        val slice2 = repo.findAll(nextPageRequest)

        assertThat(slice2).hasSize(2)
        assertThat(slice2.isFirst).isFalse()
        assertThat(slice2.isLast).isTrue()
    }

    @Test
    fun `slice vs page`() {
        repeat(10) { aUser() }

        assertThat(repo.findAll(PageRequest.of(0, 3))).hasSize(3)
        assertThat(repo.findAll(PageRequest.of(0, 3))).hasSize(3)

        assertThat(repo.findAll(PageRequest.of(0, 3))).isExactlyInstanceOf(SliceImpl::class.java)
    }
}

fun <T> Optional<T>.asNullable(): T? = if (isPresent) get() else null
