package com.example.springdatajpademo

import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import javax.persistence.EntityManager
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest
@Transactional
abstract class DbTest {

    @Autowired
    lateinit var em: EntityManager

    @Autowired
    lateinit var dao: ClassicUserDao

    @Autowired
    lateinit var repo: UserRepository

    fun aUser(firstName: String = uniqueName("First"), lastName: String = uniqueName("Last")): User {
        val user = User(firstName, lastName)
        em.persist(user)
        return user
    }
}

var userCounter = 0
fun uniqueName(prefix: String): String {
    return prefix + "-" + ++userCounter
}